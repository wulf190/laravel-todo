## Versions
**・PHP:   7.1**  
**・MySQL: 5.7**  
**・Nginx**  

## Getting started
### 1.clone & build
````
$ git clone https://github.com/takeru56/Laravel_Template.git
$ cd laravel_template
$ docker-compose run web -d --build
````    

### 2.environment

```
$ cp .env.example .env
$ docker-compose run web php artisan key:generate 
```

### 3.install composer packages
```
$ docker-compose run web composer install
```

### 4.install node_modules
```
$ docker-compose run web npm install
```
## Access

**・URL:  localhost:80/**  

```
$ npm run watch
```

